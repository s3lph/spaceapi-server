# SpaceAPI Server Changelog

<!-- BEGIN RELEASE v0.5.1 -->
## Version 0.5.1

Maintenance build

### Changes

<!-- BEGIN CHANGES 0.5.1 -->
- Fix: CI & Release process
<!-- END CHANGES 0.5.1 -->

<!-- END RELEASE v0.5.1 -->

<!-- BEGIN RELEASE v0.5 -->
## Version 0.5

Update Readme, fix systemd unit

### Changes

<!-- BEGIN CHANGES 0.5 -->
- Fix: Systemd unit now depends on multi-user.target
- Fix smaller issues in Readme and update for SpaceAPI v14
<!-- END CHANGES 0.5 -->

<!-- END RELEASE v0.5 -->

<!-- BEGIN RELEASE v0.4 -->
## Version 0.4

Replace Jinja2+JSON with PyYAML

### Changes

<!-- BEGIN CHANGES 0.4 -->
- Switch config file format from JSON to YAML
- Remove Jinja2, replaced with YAML custom tags for plugin calls
<!-- END CHANGES 0.4 -->

<!-- END RELEASE v0.4 -->

<!-- BEGIN RELEASE v0.3.1 -->
## Version 0.3

Container image release

### Changes

<!-- BEGIN CHANGES 0.3.1 -->
- Release as container image
<!-- END CHANGES 0.3.1 -->

<!-- END RELEASE v0.3.1 -->

<!-- BEGIN RELEASE v0.3 -->
## Version 0.3

Feature Removal Release

### Changes

<!-- BEGIN CHANGES 0.3 -->
- Remove template_test decorator
<!-- END CHANGES 0.3 -->

<!-- END RELEASE v0.3 -->

<!-- BEGIN RELEASE v0.2 -->
## Version 0.2

Bugfix Release

### Changes

<!-- BEGIN CHANGES 0.2 -->
- Fix: Template was partially overwritten and not updated until a server restart.
<!-- END CHANGES 0.2 -->

<!-- END RELEASE v0.2 -->

<!-- BEGIN RELEASE v0.1 -->
## Version 0.1

First release.

### Changes

<!-- BEGIN CHANGES 0.1 -->
- First somewhat stable version.
<!-- END CHANGES 0.1 -->

<!-- END RELEASE v0.1 -->
