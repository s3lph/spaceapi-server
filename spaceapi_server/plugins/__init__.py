
import yaml

from spaceapi_server import config, template


def template_function(fn):
    """
    Register the decorated function as a callable template function.
    :param fn: The function to register.
    """
    yaml.SafeLoader.add_constructor(f'!{fn.__name__}', template.plugin_constructor)
    template.PluginInvocation.register_plugin(fn.__name__, fn)
    return fn


def get_plugin_config(name: str):
    """
    Return a plugin's configuration under .plugins[name]
    :param name: The plugin name.
    """
    return config.get().get('plugins', {}).get(name, {})
