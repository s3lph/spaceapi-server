
import yaml


# The parsed config object
__CONFIG = {}


def load(filename: str) -> None:
    """
    Load a YAML-formatted configuration file.
    :param filename: The config file to load.
    """
    global __CONFIG
    # Open and parse the YAML config file
    with open(filename, 'r') as conf:
        __CONFIG = yaml.safe_load(conf)


def get() -> dict:
    """
    Return the current configuration.
    """
    global __CONFIG
    return __CONFIG
