import os
from spaceapi_server import config, plugins

@plugins.template_function
# The plugin can be invoked by using the !space_state YAML tag
def space_state():
    # Get the plugin config dict
    conf = config.get_plugin_config('filestate')
    # Get the filename, default to /var/space_state
    filename = conf.get('filename', '/var/space_state')
    try:
        # Get the file's properties
        stat = os.stat(filename)
    except FileNotFoundError:
        # File doesn't exist, aka. space is closed
        return {
            'open': False
        }
    # File exists, aka. space is open.  Also report the mtime as "last changed" timestamp
    return {
        'open': True,
        'lastchange': int(stat.st_mtime)
    }
