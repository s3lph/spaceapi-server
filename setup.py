#!/usr/bin/env python3

from setuptools import setup, find_packages

from spaceapi_server import __version__

setup(
    name='spaceapi_server',
    version=__version__,
    author='s3lph',
    author_email='account-gitlab-ideynizv@kernelpanic.lol',
    description='Lightweight SpaceAPI (spaceapi.io) endpoint server',
    license='MIT',
    keywords='spaceapi',
    url='https://gitlab.com/s3lph/spaceapi-server',
    packages=find_packages(exclude=['*.test']),
    long_description='Lightweight server for SpaceAPI endpoints. Includes support for pluggable templating, '
                     'so dynamic content, like sensor values, can be added.',
    python_requires='>=3.6',
    install_requires=[
        'bottle',
        'PyYAML',
    ],
    entry_points={
        'console_scripts': [
            'spaceapi-server = spaceapi_server:start'
        ]
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.9',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application'
    ]
)
